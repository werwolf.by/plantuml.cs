The package provides classes to interact with PlantUML.
It is based on .NET Standard.

## PlantUMLURL 

The class can transform a plain, textual PlantUML diagram to a public available 
URL, where everyone can edit and view the diagram as png or svg graphic.

The class provides three static functions:

```c#
PlantUMLURL.UML(String content);
PlantUMLURL.SVG(String content);
PlantUMLURL.PNG(String content);
PlantUMLURL.ASCII(String content);
```

For example a plain PlantUML content like

```
@startuml
Bob -> Alice : hello
@enduml
```
will be transformed into 
http://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000 

or (the SVG image)
http://www.plantuml.com/plantuml/svg/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000

or (the PNG image)
http://www.plantuml.com/plantuml/png/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000

or (the ASCII art representation)
http://www.plantuml.com/plantuml/txt/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000

respectively.


Instead of the static convenience methods you can also use special, builder like methods:

```c#
PlantUMLURL.Create()
    .WithBaseUrl("http://www.plantuml.com/plantuml")
    .WithUmlContent(content)
    .UmlStyle()
    .ToString();
```