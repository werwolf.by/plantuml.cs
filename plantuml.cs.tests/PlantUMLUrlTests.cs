﻿using System;
using Iternity.PlantUML;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace plantuml.cs.tests
{
    [TestClass]
    public class PlantUMLUrlTests
    {
        private static String ALICE_AND_BOB_SEQUENCE = "@startuml\nBob -> Alice : hello\n@enduml";

        [TestMethod]
        public void defaultLicenseDiagram()
        {
            String url = PlantUMLUrl.Create().ToString();
            Assert.AreEqual("http://plantuml.com/plantuml/uml/SoWkIImgAStDuSh9J4xDAqxbSaZDIm7o0G00", url);
        }

        [TestMethod]
        public void bobAndAliceSequenceDiagramAsSVG()
        {
            String url = PlantUMLUrl.SVG(ALICE_AND_BOB_SEQUENCE);
            Assert.AreEqual("http://plantuml.com/plantuml/svg/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void bobAndAliceSequenceDiagramAsPNG()
        {
            String url = PlantUMLUrl.PNG(ALICE_AND_BOB_SEQUENCE);
            Assert.AreEqual("http://plantuml.com/plantuml/png/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void bobAndAliceSequenceDiagramAsUML()
        {
            String url = PlantUMLUrl.UML(ALICE_AND_BOB_SEQUENCE);
            Assert.AreEqual("http://plantuml.com/plantuml/uml/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void bobAndAliceSequenceDiagramAsASCII()
        {
            String url = PlantUMLUrl.ASCII(ALICE_AND_BOB_SEQUENCE);
            Assert.AreEqual("http://plantuml.com/plantuml/txt/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void bobAndAliceSequenceDiagramWithLocalBaseUrl()
        {
            String url = PlantUMLUrl.Create().WithBaseUrl("http://localhost:8080/plantuml").UmlStyle().WithUmlContent(ALICE_AND_BOB_SEQUENCE).ToString();
            Assert.AreEqual("http://localhost:8080/plantuml/uml/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }
    }
}
