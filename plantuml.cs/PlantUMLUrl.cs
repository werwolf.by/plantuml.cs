﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Iternity.PlantUML
{
    /// <summary>
    /// Create a Links to a PlantUML from plain PlantUML content.
    /// See http://plantuml.com/plantuml for examples.
    /// </summary>
    public class PlantUMLUrl
    {
        
        /// <summary>
        /// Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/svg/{content}
        /// If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
        /// </summary>
        /// <param name="content">PlantUML text</param>
        /// <returns>self contained PlantUML link http://plantuml.com/plantuml/svg/{content}</returns>
        public static String SVG(String content)
        {
            return PlantUMLUrl.Create()
                    .WithUmlContent(content)
                    .SvgStyle()
                    .ToString();
        }

        /// <summary>
        /// Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/png/{content}
        /// If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
        /// </summary>
        /// <param name="content">PlantUML text</param>
        /// <returns>self contained PlantUML link http://plantuml.com/plantuml/png/{content}</returns>
        public static String PNG(String content)
        {
            return PlantUMLUrl.Create()
                    .WithUmlContent(content)
                    .PngStyle()
                    .ToString();
        }

        /// <summary>
        /// Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/uml/{content}
        /// If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
        /// </summary>
        /// <param name="content">PlantUML text</param>
        /// <returns>self contained PlantUML link http://plantuml.com/plantuml/uml/{content}</returns>
        public static String UML(String content)
        {
            return PlantUMLUrl.Create()
                    .WithUmlContent(content)
                    .UmlStyle()
                    .ToString();
        }

        /// <summary>
        /// Convenient method to create a self contained PlantUML link http://plantuml.com/plantuml/txt/{content}
        /// If you want to use another base url than http://plantuml.com/plantuml please use the public ctor.
        /// </summary>
        /// <param name="content">PlantUML text</param>
        /// <returns>self contained PlantUML link http://plantuml.com/plantuml/txt/{content}</returns>
        public static String ASCII(String content)
        {
            return PlantUMLUrl.Create()
                    .WithUmlContent(content)
                    .AsciiStyle()
                    .ToString();
        }

        public static PlantUMLUrl Create()
        {
            return new PlantUMLUrl();
        }

        private String baseUrl = "http://plantuml.com/plantuml";

        private String umlContent = "@startuml\nlicense\n@enduml";

        private String kind = "uml";


        public PlantUMLUrl WithBaseUrl(String baseUrl)
        {
            this.baseUrl = baseUrl;
            return this;
        }

        public PlantUMLUrl WithUmlContent(String umlContent)
        {
            this.umlContent = umlContent;
            return this;
        }

        public PlantUMLUrl PngStyle()
        {
            this.kind = "png";
            return this;
        }

        public PlantUMLUrl SvgStyle()
        {
            this.kind = "svg";
            return this;
        }

        public PlantUMLUrl UmlStyle()
        {
            this.kind = "uml";
            return this;
        }

        public PlantUMLUrl AsciiStyle()
        {
            this.kind = "txt";
            return this;
        }

        public override String ToString()
        {
            var content = this.umlContent;
            content = Uri.UnescapeDataString(content);
            var bytes = zipString(content);
            var base64 = encode64(bytes);
            return $"{baseUrl}/{kind}/{base64}";
        }

        #region Zipping
        private static byte[] zipString(string str)
        {
            using (var output = new MemoryStream())
            {
                using (var gzip =
                    new DeflateStream(output, CompressionMode.Compress))
                {
                    using (var writer =
                        new StreamWriter(gzip, new UTF8Encoding(false)))
                    {
                        writer.Write(str);
                    }
                }
                return output.ToArray();
            }
        }
        #endregion

        #region PlantUML Base64 Encoding
        private static string encode64(byte[] data)
        {
            var r = "";
            for (var i = 0; i < data.Length; i += 3)
            {
                if (i + 2 == data.Length)
                {
                    r += append3bytes(data[i], data[i + 1], 0);
                }
                else if (i + 1 == data.Length)
                {
                    r += append3bytes(data[i], 0, 0);
                }
                else
                {
                    r += append3bytes(data[i], data[i + 1],
                        data[i + 2]);
                }
            }
            return r;
        }

        private static string append3bytes(int b1, int b2, int b3)
        {
            var c1 = b1 >> 2;
            var c2 = ((b1 & 0x3) << 4) | (b2 >> 4);
            var c3 = ((b2 & 0xF) << 2) | (b3 >> 6);
            var c4 = b3 & 0x3F;
            var r = "";
            r += encode6bit(c1 & 0x3F);
            r += encode6bit(c2 & 0x3F);
            r += encode6bit(c3 & 0x3F);
            r += encode6bit(c4 & 0x3F);
            return r;
        }

        private static char encode6bit(int b)
        {
            if (b < 10)
            {
                return (char)(48 + b);
            }
            b -= 10;
            if (b < 26)
            {
                return (char)(65 + b);
            }
            b -= 26;
            if (b < 26)
            {
                return (char)(97 + b);
            }
            b -= 26;
            if (b == 0) return '-';
            return b == 1 ? '_' : '?';
        }
        #endregion
    }

}
